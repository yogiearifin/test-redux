import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getJson } from "../store/actions/reducerJson";

const ReducerTest = () => {
  const dataDariReducer = useSelector((state) => state.reducerJson.json); // useselector untuk mengambil data dari reducer setelah diget
  // state = iterator(nama bebas), reducerjson = nama file reducernya, json = nama objek data dari reducer yang berisi data yang di get
  const dispatch = useDispatch(); // untuk mendispatch action di page
  console.log(dataDariReducer);

  useEffect(() => { // useefect untuk menjalankan fungsi saat page di load JIKA use effect tidak punya dependensi
    dispatch(getJson());
  }, []); // [] / array kosong ini adalah dependensi, apabila diisi maka fungsi di dalam useeffect akan dijalankan saat terjadi perubahan di dependensi
  return (
    <div>
      <h1>INI REDUCER</h1>
      {dataDariReducer.map((data, index) => ( // mapping data yang di munculkan di page yang berasal dari reducer
        <div key={index}>
          <p>{index}</p>
          <p>{data.body}</p>
          <p>{data.title}</p>
        </div>
      ))}
    </div>
  );
};

export default ReducerTest;
