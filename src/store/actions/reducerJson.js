import { GET_JSON_BEGIN, GET_JSON_SUCCESS, GET_JSON_FAIL } from "./types"; // type dari types untuk menghindari typo
import axios from "axios"; // axios untuk get data

export const getJson = () => async (dispatch) => {
  dispatch({
    type: GET_JSON_BEGIN,
    loading: true,
    error: null,
  });
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/posts");
    console.log("res dari action", res); // hasil response get data dari url di atas apabila sukses
    dispatch({
      type: GET_JSON_SUCCESS,
      loading: false,
      payload: res.data, // data yang nantinya dimasukkan di reducer
      error: null,
    });
  } catch (error) {
    dispatch({
      type: GET_JSON_FAIL,
      error: error.response,
    });
    console.log(error);
  }
};
