export const GET_JSON_BEGIN = "GET_JSON_BEGIN";
export const GET_JSON_SUCCESS = "GET_JSON_SUCCESS";
export const GET_JSON_FAIL = "GET_JSON_FAIL";

export const POST_JSON_BEGIN = "POST_JSON_BEGIN";
export const POST_JSON_SUCCESS = "POST_JSON_SUCCESS";
export const POST_JSON_FAIL = "POST_JSON_FAIL";

// file type ini untuk menghindari type yang nantinya file ini akan diimport di file action atau reducer
