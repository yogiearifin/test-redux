import {
  GET_JSON_BEGIN,
  GET_JSON_SUCCESS,
  GET_JSON_FAIL,
} from "../actions/types"; // type dari action

const initialState = {
  json: [],
  loading: false,
  error: null,
}; // initial state yang nantinya berisi data yang diget dari action

const reducerJson = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      }; // default reducer saat dia diload
    case GET_JSON_BEGIN:
      return {
        ...state,
        loading: true,
      }; // seleksi perubahan dari type. disini objek loading menjadi true saat type get_json_begin terpannggil dari action
    case GET_JSON_SUCCESS:
      return {
        json: payload,
        loading: false,
      }; // sama seperti di atas
    case GET_JSON_FAIL:
      return {
        json: [],
        loading: false,
        error: error,
      }; // sama seperti di atas
  }
};

export default reducerJson;
