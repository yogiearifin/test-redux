import { combineReducers } from "redux";
import reducerJson from "./reducerJson";

const rootReducers = combineReducers({
  reducerJson, // list dari reducer yang di daftarkan di index. setelah membuat file reducer baru wajib didaftarkan disini agar terbaca di store / app.js
});

export default rootReducers;
