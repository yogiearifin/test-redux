import logo from "./logo.svg";
import "./App.css";
import store from "./store"; // file index.js dari folder store
import { Provider } from "react-redux"; // provider berfungsi untuk menghubungkan antara redux dengan app js
import ReducerTest from "./component/ReducerTest";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <ReducerTest />
      </div>
    </Provider>
  );
}

export default App;
